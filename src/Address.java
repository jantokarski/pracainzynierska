/**
 * Created by Kamil on 2017-11-14.
 */
public class Address {
    private String its_address;

    public Address() {
        this.its_address = null;
    }

    public Address(String its_address) {
        this.its_address = its_address;
    }

    public String getIts_address() {
        return its_address;
    }

    public void setIts_address(String its_address) {
        this.its_address = its_address;
    }

    @Override
    public String toString() {
        return its_address;
    }
}
