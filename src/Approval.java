/**
 * Created by Kamil on 2017-11-14.
 */
public class Approval {
    private String its_status;

    public Approval() {
        this.its_status = null;
    }

    public Approval(String its_status) {
        this.its_status = its_status;
    }

    public String getIts_status() {
        return its_status;
    }

    public void setIts_status(String its_status) {
        this.its_status = its_status;
    }

    @Override
    public String toString() {
        return its_status;
    }
}
