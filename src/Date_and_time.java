/**
 * Created by Kamil on 2017-11-14.
 */
public class Date_and_time {
    private String its_release;

    public Date_and_time() {
        this.its_release = null;
    }

    public Date_and_time(String its_release) {
        this.its_release = its_release;
    }

    public String getIts_release() {
        return its_release;
    }

    public void setIts_release(String its_release) {
        this.its_release = its_release;
    }

    @Override
    public String toString() {
        return its_release;
    }
}
