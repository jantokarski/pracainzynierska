/**
 * Created by Kamil on 2017-11-14.
 */
public class Identifier {
    private String its_id;

    public Identifier() {
        this.its_id = null;
    }

    public Identifier(String its_id) {
        this.its_id = its_id;
    }

    public String getIts_id() {
        return its_id;
    }

    public void setIts_id(String its_id) {
        this.its_id = its_id;
    }

    @Override
    public String toString() {
        return its_id;
    }
}
