/**
 * Created by Kamil on 2017-11-14.
 */
public class Person {
    private String its_person;

    public Person() {
        this.its_person = null;
    }

    public Person(String its_person) {
        this.its_person = its_person;
    }

    public String getIts_person() {
        return its_person;
    }

    public void setIts_person(String its_person) {
        this.its_person = its_person;
    }

    @Override
    public String toString() {
        return its_person;
    }
}
