/**
 * Created by Kamil on 2017-11-14.
 */
public class Person_and_address {
    private Person its_person;
    private Address its_address;

    public Person_and_address() {
        this.its_person = null;
        this.its_address = null;
    }

    public Person getIts_person() {
        return its_person;
    }

    public void setIts_person(Person its_person) {
        this.its_person = its_person;
    }

    public Address getIts_address() {
        return its_address;
    }

    public void setIts_address(Address its_address) {
        this.its_address = its_address;
    }

    @Override
    public String toString() {
        return its_person.toString() + " " + its_address.toString();
    }
}
