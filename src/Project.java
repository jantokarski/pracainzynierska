/**
 * Created by Kamil on 2017-11-14.
 */
public class Project {
    private Identifier its_id;
    private Approval its_status;
    private Date_and_time its_release;
    private Person_and_address its_owner;

    public Project() {
        this.its_id = null;
        this.its_status = null;
        this.its_release = null;
        this.its_owner = null;
    }

    public Identifier getIts_id() {
        return its_id;
    }

    public void setIts_id(Identifier its_id) {
        this.its_id = its_id;
    }

    public Approval getIts_status() {
        return its_status;
    }

    public void setIts_status(Approval its_status) {
        this.its_status = its_status;
    }

    public Date_and_time getIts_release() {
        return its_release;
    }

    public void setIts_release(Date_and_time its_release) {
        this.its_release = its_release;
    }

    public Person_and_address getIts_owner() {
        return its_owner;
    }

    public void setIts_owner(Person_and_address its_owner) {
        this.its_owner = its_owner;
    }

    @Override
    public String toString() {
        return its_id.toString() + " " + its_status.toString() + " " + its_release.toString() + " " + its_owner.toString();
    }
}